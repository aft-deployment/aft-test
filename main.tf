# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0
#
module "aft" {
  source = "github.com/aws-ia/terraform-aws-control_tower_account_factory"
  ct_management_account_id    = var.ct_management_account_id
  log_archive_account_id      = var.log_archive_account_id
  audit_account_id            = var.audit_account_id
  aft_management_account_id   = var.aft_management_account_id
  ct_home_region              = var.ct_home_region
  tf_backend_secondary_region = var.tf_backend_secondary_region

  # VCS Vars
  vcs_provider                                  = "bitbucket"
  account_request_repo_name                     = "${var.bitbucket_username}/aft-account-request"
  global_customizations_repo_name               = "${var.bitbucket_username}/aft-account-global-customizations"
  account_customizations_repo_name              = "${var.bitbucket_username}/aft-account_customizations"
  account_provisioning_customizations_repo_name = "${var.bitbucket_username}/aft-account_provisioning_customizations"
  # TF Vars
  terraform_distribution = "tfc"
  terraform_token        = var.terraform_token 
  terraform_org_name     = var.terraform_org_name
}